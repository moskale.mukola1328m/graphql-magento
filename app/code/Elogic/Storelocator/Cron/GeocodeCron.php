<?php

namespace Elogic\Storelocator\Cron;

use Elogic\Storelocator\Helper\Data;
use Elogic\Storelocator\Helper\GeoCode;
use Elogic\Storelocator\Model\ResourceModel\Storelocator\CollectionFactory;
use Elogic\Storelocator\Model\StorelocatorRepository;
use Magento\Framework\Exception\CouldNotSaveException;

class GeocodeCron
{
    protected $logger;
    /**
     * @var GeoCode
     */
    private $geoCode;
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var Data
     */
    private $data;
    /**
     * @var StorelocatorRepository
     */
    private $storelocatorRepository;

    /**
     * GeocodeCron constructor.
     * @param \Psr\Log\LoggerInterface $logger
     * @param GeoCode $geoCode
     * @param CollectionFactory $collectionFactory
     * @param Data $data
     * @param StorelocatorRepository $storelocatorRepository
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        GeoCode $geoCode,
        CollectionFactory $collectionFactory,
        Data $data,
        StorelocatorRepository $storelocatorRepository
    ) {
        $this->logger = $logger;
        $this->geoCode = $geoCode;
        $this->collectionFactory = $collectionFactory;
        $this->data = $data;
        $this->storelocatorRepository = $storelocatorRepository;
    }

    public function execute()
    {
        if (!$this->data->getCronStatus()) {
            return;
        }

        $collection = $this->collectionFactory->create();
        $collection->addFieldToFilter(
            ['longitude', 'latitude'],
            [
                ['eq' => '0'],
                ['eq' => '0']
            ]
        );

        foreach ($collection->getData() as $store) {
            try {
                /** @var \Elogic\Storelocator\Model\Storelocator $store */
                $address = $store->getAddress() . ',' . $store->getCity() . ',' . $store->getState() . ',' . $store->getZip();
                $geocode = $this->geoCode->getGeocode($address);
                $store->setLongitude($geocode['longitude'])->setLatitude($geocode['latitude']);
                $this->storelocatorRepository->save($store);
            } catch (CouldNotSaveException $e) {
                $this->logger->info($e->getMessage() . 'in store_id: ' . $store->getId());
            }
        }
    }
}
