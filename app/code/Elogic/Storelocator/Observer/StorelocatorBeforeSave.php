<?php

namespace Elogic\Storelocator\Observer;

use Elogic\Storelocator\Helper\Data;
use Elogic\Storelocator\Helper\GeoCode;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Message\ManagerInterface;

class StorelocatorBeforeSave implements ObserverInterface
{
    /**
     * @var GeoCode
     */
    private $geoCode;
    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    private $messageManager;
    /**
     * @var Data
     */
    private $data;

    /**
     * StorelocatorBeforeSave constructor.
     * @param Data $data
     * @param GeoCode $geoCode
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     */
    public function __construct(
        Data $data,
        GeoCode $geoCode,
        ManagerInterface $messageManager
    ) {
        $this->geoCode = $geoCode;
        $this->messageManager = $messageManager;
        $this->data = $data;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        if (!$this->data->getObserverStatus()) {
            return;
        }

        /** @var \Elogic\Storelocator\Model\Storelocator $store */
        $store = $observer->getEvent()->getObject();

        if ((doubleval($store->getLatitude()) == 0) || (doubleval($store->getLongitude()) == 0)) {
            $address = $store->getAddress() . ',' . $store->getCity() . ',' . $store->getState() . ',' . $store->getZip();
            try {
                $geocode = $this->geoCode->getGeocode($address);
                $store->setLongitude($geocode['longitude'])->setLatitude($geocode['latitude']);
            } catch (CouldNotSaveException $exception) {
                $this->messageManager->addNoticeMessage($exception->getMessage());
            }
        }
    }
}
