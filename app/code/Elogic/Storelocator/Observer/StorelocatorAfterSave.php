<?php

namespace Elogic\Storelocator\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\UrlRewrite\Model\ResourceModel\UrlRewriteCollection;
use Magento\UrlRewrite\Model\UrlRewriteFactory;

class StorelocatorAfterSave implements ObserverInterface
{
    const TARGET_PATH = 'storelocator/stores/index/url_key/';
    const REQUEST_PATH = 'storelocator/stores/';

    /**
     * @var UrlRewriteFactory
     */
    private $urlRewriteFactory;
    /**
     * @var UrlRewriteCollection
     */
    private $urlRewriteCollection;

    /**
     * StorelocatorAfterSave constructor.
     * @param UrlRewriteFactory $urlRewriteFactory
     * @param UrlRewriteCollection $urlRewriteCollection
     */
    public function __construct(
        UrlRewriteFactory $urlRewriteFactory,
        UrlRewriteCollection $urlRewriteCollection
    ) {
        $this->urlRewriteFactory = $urlRewriteFactory;
        $this->urlRewriteCollection = $urlRewriteCollection;
    }

    /**
     * @param Observer $observer
     * @throws \Exception
     */
    public function execute(Observer $observer)
    {
        /** @var \Elogic\Storelocator\Model\Storelocator $store */
        $store = $observer->getEvent()->getObject();

        $urlRewriteCollection = $this->urlRewriteCollection->addFieldToFilter('target_path', ['eq' => self::TARGET_PATH . $store->getUrlKey()]);
        $data = $urlRewriteCollection->getData();
        if (!empty($data)) {
            return;
        }

        $urlRewriteModel = $this->urlRewriteFactory->create();
        $urlRewriteModel->setStoreId(1);
        $urlRewriteModel->setIsSystem(0);
        $urlRewriteModel->setTargetPath(self::TARGET_PATH . $store->getUrlKey());
        $urlRewriteModel->setRequestPath(self::REQUEST_PATH . $store->getUrlKey());
        $urlRewriteModel->save();
    }
}
