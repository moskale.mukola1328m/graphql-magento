<?php

namespace Elogic\Storelocator\Model\Storelocator;

use Elogic\Storelocator\Model\ResourceModel\Storelocator\CollectionFactory;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Ui\DataProvider\Modifier\PoolInterface;

/**
 * Class DataProvider
 */
class DataProvider extends \Magento\Ui\DataProvider\ModifierPoolDataProvider
{
    /**
     * @var Elogic\Storelocator\Model\ResourceModel\Storelocator\Collection
     */
    protected $collection;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var array
     */
    protected $loadedData;
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * DataProvider constructor.
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $storelocatorCollectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param StoreManagerInterface $storeManager
     * @param array $meta
     * @param array $data
     * @param PoolInterface|null $pool
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $storelocatorCollectionFactory,
        DataPersistorInterface $dataPersistor,
        StoreManagerInterface $storeManager,
        array $meta = [],
        array $data = [],
        PoolInterface $pool = null
    ) {
        $this->collection = $storelocatorCollectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data, $pool);
        $this->storeManager = $storeManager;
    }

    /**
     * Get data
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        /** @var \Elogic\Storelocator\Model\Storelocator $store */
        foreach ($items as $store) {
            $this->loadedData[$store->getId()] = $store->getData();
            if ($store->getImages()) {
                $images_array = explode(';', $store->getImages());

                for ($i = 0; $i < count($images_array); $i++) {
                    $m['images'][$i]['images'] = $images_array[$i];
                    $m['images'][$i]['url'] = $this->getMediaUrl() . $images_array[$i];
                }

                $fullData = $this->loadedData;
                $this->loadedData[$store->getId()] = array_merge($fullData[$store->getId()], $m);
            }
        }

        $data = $this->dataPersistor->get('store_locator');
        if (!empty($data)) {
            $store = $this->collection->getNewEmptyItem();
            $store->setData($data);
            $this->loadedData[$store->getId()] = $store->getData();
            $this->dataPersistor->clear('store_locator');
        }

        return $this->loadedData;
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getMediaUrl()
    {
        $mediaUrl = $this->storeManager->getStore()
                ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'storelocator/images/';
        return $mediaUrl;
    }
}
