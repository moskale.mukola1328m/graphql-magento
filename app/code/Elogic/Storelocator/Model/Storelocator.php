<?php

namespace Elogic\Storelocator\Model;

use Elogic\Storelocator\Api\Data\StorelocatorInterface;
use Magento\Framework\Model\AbstractModel;

class Storelocator extends AbstractModel implements StorelocatorInterface
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'storelocator_model';

    /**
     * Construct
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Elogic\Storelocator\Model\ResourceModel\Storelocator::class);
    }
    /**
     * Retrieve Store Review id
     *
     * @return int
     */
    public function getId()
    {
        return $this->getData(self::STORE_ID);
    }

    /**
     * Retrieve Customer Name
     * @return string
     */
    public function getStoreName()
    {
        return $this->getData(self::STORE_NAME);
    }

    /**
     * Get Content
     * @return mixed|string|null
     */
    public function getDescription()
    {
        return $this->getData(self::DESCRIPTION);
    }

    /**
     * Get Update Time
     * @return mixed|string|null
     */
    public function getImages()
    {
        return $this->getData(self::IMAGES);
    }

    /**
     * Get City
     * @return string|void|null
     */
    public function getCity()
    {
        return $this->getData(self::CITY);
    }

    /**
     * Get ZIP
     * @return string|void|null
     */
    public function getZip()
    {
        return $this->getData(self::ZIP);
    }

    /**
     * Get State
     * @return string|void|null
     */
    public function getState()
    {
        return $this->getData(self::STATE);
    }

    /**
     * Get Address
     * @return string|void|null
     */
    public function getAddress()
    {
        return $this->getData(self::ADDRESS);
    }

    /**
     * Get url key
     * @return string
     */
    public function getUrlKey()
    {
        return $this->getData(self::URL_KEY);
    }

    /** Work Schedule
     * Get is Active
     * @return string
     */
    public function getWorkSchedule()
    {
        return $this->getData(self::WORK_SCHEDULE);
    }

    /**
     * Get is Active
     * @return double
     */
    public function getLongitude()
    {
        return $this->getData(self::LONGITUDE);
    }

    /**
     * Get is Active
     * @return double
     */
    public function getLatitude()
    {
        return $this->getData(self::LATITUDE);
    }

    /**
     * Set ID
     * @param mixed $id
     * @return StorelocatorInterface
     */
    public function setId($id)
    {
        return $this->setData(self::STORE_ID, $id);
    }

    /**
     * Set store name
     * @param $storeName
     * @return StorelocatorInterface
     */
    public function setStoreName($storeName)
    {
        return $this->setData(self::STORE_NAME, $storeName);
    }

    /**
     * Set description name
     * @param $description
     * @return StorelocatorInterface
     */
    public function setDescription($description)
    {
        return $this->setData(self::DESCRIPTION, $description);
    }

    /**
     * Set Images
     * @param $images
     * @return StorelocatorInterface
     */
    public function setImages($images)
    {
        return $this->setData(self::IMAGES, $images);
    }

    /**
     * Set city
     * @param $city
     * @return StorelocatorInterface|void
     */
    public function setCity($city)
    {
        return $this->setData(self::CITY, $city);
    }

    /**
     * Set ZIP
     * @param $zip
     * @return StorelocatorInterface|void
     */
    public function setZip($zip)
    {
        return $this->setData(self::ZIP, $zip);
    }

    /**
     * Set state
     * @param $state
     * @return StorelocatorInterface|void
     */
    public function setState($state)
    {
        return $this->setData(self::STATE, $state);
    }

    /**
     * Set address
     * @param $address
     * @return StorelocatorInterface|void
     */
    public function setAddress($address)
    {
        return $this->setData(self::ADDRESS, $address);
    }

    /**
     * Set Url Key
     * @param $urlKey
     * @return StorelocatorInterface
     */
    public function setUrlKey($urlKey)
    {
        return $this->setData(self::URL_KEY, $urlKey);
    }

    /**
     * Set Work Schedule
     * @param $workSchedule
     * @return StorelocatorInterface
     */
    public function setWorkSchedule($workSchedule)
    {
        return $this->setData(self::WORK_SCHEDULE, $workSchedule);
    }

    /**
     * Set Longitude
     * @param $longitude
     * @return StorelocatorInterface|void
     */
    public function setLongitude($longitude)
    {
        return $this->setData(self::LONGITUDE, $longitude);
    }

    /**
     * Set Latitude
     * @param $latitude
     * @return StorelocatorInterface|void
     */
    public function setLatitude($latitude)
    {
        return $this->setData(self::LATITUDE, $latitude);
    }
}
