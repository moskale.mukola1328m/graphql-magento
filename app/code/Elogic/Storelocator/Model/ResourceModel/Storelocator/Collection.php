<?php

namespace Elogic\Storelocator\Model\ResourceModel\Storelocator;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'store_id';

    /**
     * Construct collection
     */
    public function _construct()
    {
        $this->_init(\Elogic\Storelocator\Model\Storelocator::class, \Elogic\Storelocator\Model\ResourceModel\Storelocator::class);
    }
}
