<?php

namespace Elogic\Storelocator\Model\Resolver;

use Elogic\Storelocator\Model\ResourceModel\Storelocator\Collection;
use Elogic\Storelocator\Model\StorelocatorRepository;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

/**
 * Book field resolver, used for GraphQL request processing
 */
class StorelocatorAll implements ResolverInterface
{
    /**
     * @var Collection
     */
    private $collection;

    public function __construct(
        Collection $collection
    ) {
        $this->collection = $collection;
    }

    /**
     * @inheritdoc
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        try {
            $storeData['items'] = $this->collection->load()->getData();

        } catch (NoSuchEntityException $e) {
            throw new GraphQlNoSuchEntityException(__($e->getMessage()), $e);
        }
        return $storeData;
    }
}
