<?php

namespace Elogic\Storelocator\Model\Resolver\Mutation;

use Elogic\Storelocator\Model\StorelocatorFactory;
use Elogic\Storelocator\Model\StorelocatorRepository;
use Magento\Framework\Exception\AuthenticationException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlAuthorizationException;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

class Create implements ResolverInterface
{

    /**
     * @var StorelocatorFactory
     */
    private $storelocatorFactory;
    /**
     * @var StorelocatorRepository
     */
    private $storelocatorRepository;

    /**
     * Create constructor.
     * @param StorelocatorFactory $storelocatorFactory
     * @param StorelocatorRepository $storelocatorRepository
     */
    public function __construct(
        StorelocatorFactory $storelocatorFactory,
        StorelocatorRepository $storelocatorRepository
    ) {
        $this->storelocatorFactory = $storelocatorFactory;
        $this->storelocatorRepository = $storelocatorRepository;
    }

    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        $store = $this->storelocatorFactory->create();
        try {
            if (isset($args['store_name'])) {
                $store->setStoreName($args['store_name']);
            } else {
                throw new GraphQlInputException(__('"author_email" value should be specified'));
            }

            if (isset($args['description'])) {
                $store->setDescription($args['description']);
            }

            if (isset($args['images'])) {
                $store->setImages($args['images']);
            }

            if (isset($args['city'])) {
                $store->setCity($args['city']);
            } else {
                throw new GraphQlInputException(__('"city" value should be specified'));
            }

            if (isset($args['zip'])) {
                $store->setZip($args['zip']);
            } else {
                throw new GraphQlInputException(__('"zip" value should be specified'));
            }

            if (isset($args['state'])) {
                $store->setState($args['state']);
            } else {
                throw new GraphQlInputException(__('"state" value should be specified'));
            }

            if (isset($args['address'])) {
                $store->setAddress($args['address']);
            } else {
                throw new GraphQlInputException(__('"address" value should be specified'));
            }

            if (isset($args['url_key'])) {
                $store->setUrlKey($args['url_key']);
            } else {
                throw new GraphQlInputException(__('"url" value should be specified'));
            }

            if (isset($args['work_schedule'])) {
                $store->setWorkSchedule($args['work_schedule']);
            }

            if (isset($args['longitude'])) {
                $store->setLongitude($args['longitude']);
            }

            if (isset($args['latitude'])) {
                $store->setLatitude($args['latitude']);
            }

            $this->storelocatorRepository->save($store);
            return ['item' => $store->getData()];
        } catch (AuthenticationException $e) {
            throw new GraphQlAuthorizationException(
                __($e->getMessage())
            );
        }
    }
}
