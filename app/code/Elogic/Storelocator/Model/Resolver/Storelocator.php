<?php

namespace Elogic\Storelocator\Model\Resolver;

use Elogic\Storelocator\Model\ResourceModel\Storelocator\Collection;
use Elogic\Storelocator\Model\StorelocatorRepository;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

/**
 * Book field resolver, used for GraphQL request processing
 */
class Storelocator implements ResolverInterface
{

    /**
     * @var StorelocatorRepository
     */
    private $storelocatorRepository;
    /**
     * @var Collection
     */
    private $collection;

    public function __construct(
        StorelocatorRepository $storelocatorRepository,
        Collection $collection
    ) {
        $this->storelocatorRepository = $storelocatorRepository;
        $this->collection = $collection;
    }

    /**
     * @inheritdoc
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        $storeId = $this->getStoreId($args);
        return $this->getStoreData($storeId);
    }

    /**
     * @param array $args
     * @return int
     * @throws GraphQlInputException
     */

    private function getStoreId(array $args): int
    {
        if (!isset($args['id'])) {
            throw new GraphQlInputException(__('"book id should be specified'));
        }
        return (int)$args['id'];
    }

    /**
     * @param int $storeId
     * @return array
     * @throws GraphQlNoSuchEntityException
     */

    private function getStoreData(int $storeId): array
    {
        try {
            $storeData = $this->collection->addFieldToFilter(
                'store_id',
                ['eq' => $storeId]
            )->getData()[0];
        } catch (NoSuchEntityException $e) {
            throw new GraphQlNoSuchEntityException(__($e->getMessage()), $e);
        }
        return $storeData;
    }
}
