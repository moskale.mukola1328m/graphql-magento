<?php

namespace Elogic\Storelocator\Model;

use Elogic\Storelocator\Api\Data\StorelocatorInterface;
use Elogic\Storelocator\Api\StorelocatorRepositoryInterface;
use Elogic\Storelocator\Model\ResourceModel\Storelocator as ResourceStorelocator;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

class StorelocatorRepository implements StorelocatorRepositoryInterface
{
    /**
     * @var ResourceStorelocator
     */
    protected $resource;

    /**
     * @var StorelocatorFactory
     */
    protected $storelocatorFactory;

    /**
     * StorelocatorRepository constructor.
     * @param ResourceStorelocator $resourceStorelocator
     * @param StorelocatorFactory $storelocatorFactory
     */
    public function __construct(
        ResourceStorelocator $resourceStorelocator,
        StorelocatorFactory $storelocatorFactory
    ) {
        $this->resource = $resourceStorelocator;
        $this->storelocatorFactory = $storelocatorFactory;
    }

    /**
     * @param StorelocatorInterface $storelocator
     * @return StorelocatorInterface|Storelocator
     * @throws CouldNotSaveException
     */
    public function save(StorelocatorInterface $storelocator)
    {
        try {
            /** @var $storelocator \Elogic\Storelocator\Model\Storelocator */
            $this->resource->save($storelocator);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        return $storelocator;
    }

    /**
     * @param int $storelocatorId
     * @return StorelocatorInterface|Storelocator
     * @throws NoSuchEntityException
     */
    public function getById($storelocatorId)
    {
        $storelocator = $this->storelocatorFactory->create();
        $this->resource->load($storelocator, $storelocatorId);
        if (!$storelocator->getId()) {
            throw new NoSuchEntityException(__('The store with the "%1" ID doesn\'t exist.', $storelocatorId));
        }
        return $storelocator;
    }

    /**
     * @param StorelocatorInterface $storelocator
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(StorelocatorInterface $storelocator)
    {
        try {
            /** @var $storelocator \Elogic\Storelocator\Model\Storelocator */
            $this->resource->delete($storelocator);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * @param int $storelocatorId
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($storelocatorId)
    {
        return $this->delete($this->getById($storelocatorId));
    }

    /**
     * @param $urlKey
     * @return Storelocator
     */
    public function getByUrlKey($urlKey)
    {
        $storelocator = $this->storelocatorFactory->create();
        $this->resource->load($storelocator, $urlKey, StorelocatorInterface::URL_KEY);
        return $storelocator;
    }
}
