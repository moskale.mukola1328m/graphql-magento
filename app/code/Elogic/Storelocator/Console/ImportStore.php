<?php
namespace Elogic\Storelocator\Console;

use Elogic\Storelocator\Model\StorelocatorFactory;
use Elogic\Storelocator\Model\StorelocatorRepository;
use Magento\Directory\Model\Region;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class GenerateErp
 */
class ImportStore extends Command
{
    const NAME = 'storelocator-import-CSV';
    /**
     * @var \Magento\Framework\File\Csv
     */
    protected $csv;
    /**
     * @var StorelocatorFactory
     */
    protected $storelocatorFactory;
    /**
     * @var StorelocatorRepository
     */
    protected $storelocatorRepository;
    /**
     * @var Region
     */
    private $region;

    /**
     * ImportStore constructor.
     * @param string|null $name
     * @param \Magento\Framework\File\Csv $csv
     * @param StorelocatorFactory $storelocatorFactory
     * @param StorelocatorRepository $storelocatorRepository
     * @param Region $region
     */
    public function __construct(
        string $name = null,
        \Magento\Framework\File\Csv $csv,
        StorelocatorFactory $storelocatorFactory,
        StorelocatorRepository $storelocatorRepository,
        Region $region
    ) {
        $this->csv = $csv;
        $this->storelocatorFactory = $storelocatorFactory;
        $this->storelocatorRepository = $storelocatorRepository;
        parent::__construct($name);
        $this->region = $region;
    }

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $options = [
            new InputOption(
                self::NAME,
                null,
                InputOption::VALUE_REQUIRED,
                'Import store in CSV'
            )
        ];
        $this->setDefinition($options);

        $this->setDescription('Import Store command. Please, upload store images to pub/media/storelocator/images');
        $this->addOption('path', "p", InputOption::VALUE_OPTIONAL, "Path for csv file");
        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $path = $input->getOption('path');

        if (empty($path)) {
            throw new \Exception("Invalid argument");
        }

        try {
            $csvData = $this->csv->getData($path);
            $keys = array_shift($csvData);
            $keys = array_flip($keys);

            foreach ($csvData as $value) {
                $store = $this->storelocatorFactory->create()
                    ->setStoreName($value[$keys['name']])
                    ->setCity($value[$keys['city']])
                    ->setZip($value[$keys['zip']])
                    ->setAddress($value[$keys['address']])
                    ->setDescription($value[$keys['description']])
                    ->setImages($value[$keys['store_img']])
                    ->setWorkSchedule($value[$keys['schedule']])
                    ->setUrlKey($value[$keys['id']]);

                if (intval($value[$keys['state']])) {
                    $region = $this->region->load(($value[$keys['state']]));
                    $store->setState($region->getName());
                } else {
                    $store->setState($value[$keys['state']]);
                }

                $this->storelocatorRepository->save($store);
            }
            $output->writeln('Import Done!');
        } catch (\Exception $e) {
            $output->writeln('Invalid CSV');
            $output->writeln($e->getMessage());
        }
    }
}
