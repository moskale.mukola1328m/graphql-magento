<?php

namespace Elogic\Storelocator\Controller\Stores;

use Elogic\Storelocator\Helper\Data;

use Elogic\Storelocator\Model\StorelocatorFactory;
use Elogic\Storelocator\Model\StorelocatorRepository;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\Action\HttpPostActionInterface;

class Index extends Action implements HttpGetActionInterface, HttpPostActionInterface
{

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var StorelocatorFactory
     */
    protected $storelocatorFactory;
    /**
     * @var StorelocatorRepository
     */
    protected $storelocatorRepository;
    /**
     * @var Data
     */
    private $helper;

    /**
     * Index constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param StorelocatorFactory $storelocatorFactory
     * @param StorelocatorRepository $storelocatorRepository
     * @param Data $helper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        StorelocatorFactory $storelocatorFactory,
        StorelocatorRepository $storelocatorRepository,
        Data $helper
    ) {
        $this->storelocatorFactory = $storelocatorFactory;
        $this->resultPageFactory = $resultPageFactory;
        $this->storelocatorRepository = $storelocatorRepository;
        $this->helper = $helper;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Page
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        $resultLayout = $this->resultPageFactory->create();
        return $resultLayout;
    }
}
