<?php

namespace Elogic\Storelocator\Controller\Adminhtml\Storelocator;

use Elogic\Storelocator\Api\StorelocatorRepositoryInterface;
use Magento\Backend\App\Action;

class Delete extends Action
{
    const ADMIN_RESOURCE = "Elogic_Storelocator::all";

    /**
     * @var StorelocatorRepositoryInterface
     */
    protected $storelocatorRepositoryInterface;

    /**
     * Delete constructor.
     * @param Action\Context $context
     * @param StorelocatorRepositoryInterface $storelocatorRepositoryInterface
     */
    public function __construct(
        Action\Context $context,
        StorelocatorRepositoryInterface $storelocatorRepositoryInterface
    ) {
        $this->storelocatorRepositoryInterface = $storelocatorRepositoryInterface;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Redirect|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        $id = $this->getRequest()->getParam('store_id');
        if ($id) {
            try {
                $this->storelocatorRepositoryInterface->deleteById($id);
                $this->messageManager->addSuccessMessage(__('You deleted the store.'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('*/*/');
            }
        }
        $this->messageManager->addErrorMessage(__('We can\'t find a store to delete.'));
        return $resultRedirect->setPath('*/*/');
    }
}
