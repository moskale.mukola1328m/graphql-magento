<?php

namespace Elogic\Storelocator\Controller\Adminhtml\Storelocator;

use Elogic\Storelocator\Api\StorelocatorRepositoryInterface;
use Elogic\Storelocator\Model\StorelocatorFactory;
use Magento\Framework\App\Action\HttpGetActionInterface;

/**
 * Edit CMS block action.
 */
class Edit extends \Magento\Backend\App\Action implements HttpGetActionInterface
{
    /**
     * @var StorelocatorRepositoryInterface
     */
    private $storelocatorRepository;
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var StorelocatorFactory
     */
    private $storelocatorFactory;
    /**
     * @var \Magento\Framework\Registry
     */
    private $coreRegistry;

    /**
     * Edit constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param StorelocatorRepositoryInterface $storelocatorRepository
     * @param StorelocatorFactory $storelocatorFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        StorelocatorRepositoryInterface $storelocatorRepository,
        StorelocatorFactory $storelocatorFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->storelocatorRepository = $storelocatorRepository;
        $this->storelocatorFactory = $storelocatorFactory;
        $this->coreRegistry = $coreRegistry;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Redirect|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('store_id');

        if ($id) {
            $model = $this->storelocatorRepository->getById($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This store no longer exists.'));
                /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        } else {
            $model = $this->storelocatorFactory->create();
        }

        $this->coreRegistry->register('store_locator', $model);

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();

        $resultPage->setActiveMenu('Elogic_Storelocator::all')
            ->addBreadcrumb(__('Store Locator'), __('Store Locator'));

        $resultPage->getConfig()->getTitle()->prepend(__('Store'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? "Store ID: " . $model->getId() : __('New Store'));
        return $resultPage;
    }
}
