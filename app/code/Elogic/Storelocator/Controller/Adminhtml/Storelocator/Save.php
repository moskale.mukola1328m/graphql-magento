<?php

namespace Elogic\Storelocator\Controller\Adminhtml\Storelocator;

use Elogic\Storelocator\Api\StorelocatorRepositoryInterface;
use Elogic\Storelocator\Model\StorelocatorFactory;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;

/**
 * Save store action.
 */
class Save extends \Magento\Backend\App\Action implements HttpPostActionInterface
{
    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var mixed
     */
    private $storelocatorFactory;

    /**
     * @var mixed
     */
    private $storelocatorRepository;

    /**
     * Save constructor.
     * @param Context $context
     * @param DataPersistorInterface $dataPersistor
     * @param StorelocatorFactory|null $storelocatorFactory
     * @param StorelocatorRepositoryInterface|null $storelocatorRepository
     */
    public function __construct(
        Context $context,
        DataPersistorInterface $dataPersistor,
        StorelocatorFactory $storelocatorFactory = null,
        StorelocatorRepositoryInterface $storelocatorRepository = null
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->storelocatorFactory = $storelocatorFactory
            ?: \Magento\Framework\App\ObjectManager::getInstance()->get(StorelocatorFactory::class);
        $this->storelocatorRepository = $storelocatorRepository
            ?: \Magento\Framework\App\ObjectManager::getInstance()->get(StorelocatorRepositoryInterface::class);
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            if (empty($data['store_id'])) {
                $data['store_id'] = null;
            }

            /** @var \Elogic\Storelocator\Model\Storelocator $model */
            $model = $this->storelocatorFactory->create();

            $id = $this->getRequest()->getParam('store_id');
            if ($id) {
                try {
                    $model = $this->storelocatorRepository->getById($id);
                } catch (LocalizedException $e) {
                    $this->messageManager->addErrorMessage(__('This store no longer exists.'));
                    return $resultRedirect->setPath('*/*/');
                }
            }

            try {
                $data = $this->_filterFoodData($data);
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('*/*/');
            }

            $model->setData($data);

            try {
                $this->storelocatorRepository->save($model);
                $this->messageManager->addSuccessMessage(__('You saved the store.'));
                $this->dataPersistor->clear('store_locator');
                return $resultRedirect->setPath('*/*/edit', ['store_id' => $model->getId()]);
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the store.'));
            }

            $this->dataPersistor->set('store_locator', $data);

            $model->setData($data);
            try {
                $this->storelocatorRepository->save($model);
            } catch (CouldNotSaveException $exception) {
                return $resultRedirect->setPath('*/*/edit', ['store_id' => $id]);
            }


            return $resultRedirect->setPath('*/*/edit', ['store_id' => $id]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    /**$exception->getMessage()
     * @param array $rawData
     * @return array
     */
    public function _filterFoodData(array $rawData)
    {
        $data = $rawData;

        $images_str = '';
        if (isset($data['images'])) {
            foreach ($data['images'] as $key => $image) {
                if (isset($image['name'])) {
                    $images_str = $images_str . $image['name'] . ';';
                }
                if (isset($image['images'])) {
                    $images_str = $images_str . $image['images'] . ";";
                }
            }
        }
        if (!empty($images_str)) {
            $images_str = mb_substr($images_str, 0, -1);
            $data['images'] = $images_str;
        } else {
            $data['images'] = null;
        }
        return $data;
    }
}
