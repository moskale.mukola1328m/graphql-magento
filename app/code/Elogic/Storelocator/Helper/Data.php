<?php

namespace Elogic\Storelocator\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;

class Data
{
    const ELOGIC_STORELOCATOR_SETTINGS_GENERAL_ENABLED = "storelocator_settings/general/enabled";
    const ELOGIC_STORELOCATOR_SETTINGS_GENERAL_API_KEY = "storelocator_settings/general/api_key";
    const ELOGIC_STORELOCATOR_SETTINGS_GENERATE_GEOCODE_OBSERVER = "storelocator_settings/generate_geocode/observer";
    const ELOGIC_STORELOCATOR_SETTINGS_GENERATE_GEOCODE_CRON = "storelocator_settings/generate_geocode/cron";

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * Data constructor.
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return mixed
     */
    public function getStorelocatorEnabled()
    {
        return $this->scopeConfig->getValue(self::ELOGIC_STORELOCATOR_SETTINGS_GENERAL_ENABLED);
    }

    /**
     * @return mixed
     */
    public function getApiKey()
    {
        return $this->scopeConfig->getValue(self::ELOGIC_STORELOCATOR_SETTINGS_GENERAL_API_KEY);
    }

    /**
     * @return mixed
     */
    public function getObserverStatus()
    {
        return $this->scopeConfig->getValue(self::ELOGIC_STORELOCATOR_SETTINGS_GENERATE_GEOCODE_OBSERVER);
    }

    /**
     * @return mixed
     */
    public function getCronStatus()
    {
        return $this->scopeConfig->getValue(self::ELOGIC_STORELOCATOR_SETTINGS_GENERATE_GEOCODE_CRON);
    }

}
