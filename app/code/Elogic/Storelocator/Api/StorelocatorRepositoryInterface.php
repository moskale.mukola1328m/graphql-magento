<?php

namespace Elogic\Storelocator\Api;


interface StorelocatorRepositoryInterface
{
    /**
     * Save StoreLocation.
     *
     * @param \Elogic\Storelocator\Api\Data\StorelocatorInterface $storelocation
     * @return \Elogic\Storelocator\Api\Data\StorelocatorInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(Data\StorelocatorInterface $storelocation);

    /**
     * Retrieve StoreLocation.
     *
     * @param int $storelocationId
     * @return \Elogic\Storelocator\Api\Data\StorelocatorInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($storelocationId);

    /**
     * Retrieve StoreLocation matching the specified criteria.
     *
     * @param \Elogic\Storelocator\Api\Data\StorelocatorInterface $storelocation
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */

    public function delete(Data\StorelocatorInterface $storelocation);

    /**
     * Delete StoreLocation by ID.
     *
     * @param int $storelocationId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($storelocationId);

//    public function getList(SearchCriteriaInterface $searchCriteria = null);
}
