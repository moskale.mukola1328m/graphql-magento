<?php

namespace Elogic\Storelocator\Api\Data;

interface StorelocatorInterface
{
    const STORE_ID = 'store_id';
    const STORE_NAME = 'store_name';
    const DESCRIPTION = 'description';
    const IMAGES = 'images';
    const CITY = 'city';
    const ZIP = 'zip';
    const STATE = 'state';
    const ADDRESS = 'address';
    const URL_KEY = 'url_key';
    const WORK_SCHEDULE = 'work_schedule';
    const LONGITUDE = 'longitude';
    const LATITUDE = 'latitude';

    /**
     * Get ID
     *
     * @return int|null;
     */
    public function getId();

    /**
     * Get StoreName
     * @return string;
     */
    public function getStoreName();

    /**
     *  get Description
     * @return string|null;
     */
    public function getDescription();

    /**
     * Get Images name
     * @return string|null;
     */
    public function getImages();

    /**
     * Get city
     * @return string|null;
     */
    public function getCity();

    /**
     * Get ZIP
     * @return string|null;
     */
    public function getZip();

    /**
     * Get state
     * @return string|null;
     */
    public function getState();

    /**
     * Get address
     * @return string|null;
     */
    public function getAddress();

    /**
     * Get Url Key
     * @return string|null
     */
    public function getUrlKey();

    /**
     * Get Work Schedule
     * @return string|null
     */
    public function getWorkSchedule();

    /**
     * Get Longitude
     * @return float
     */
    public function getLongitude();

    /**
     * Get Latitude
     * @return float
     */
    public function getLatitude();

    /**
     * @param $id
     * @return int
     * Set ID
     */
    public function setId($id);

    /**
     * @param $storeName
     * @return StorelocatorInterface
     * Set store name
     */
    public function setStoreName($storeName);

    /**
     * @param $description
     *@return StorelocatorInterface
     * Set Description
     */
    public function setDescription($description);

    /**
     * @param $images
     * @return StorelocatorInterface
     * Set images
     */
    public function setImages($images);

    /**
     * @param $city
     * @return StorelocatorInterface
     * Set city
     */
    public function setCity($city);

    /**
     * @param $zip
     * @return StorelocatorInterface
     * Set ZIP
     */
    public function setZip($zip);

    /**
     * @param $state
     * @return StorelocatorInterface
     * Set State
     */
    public function setState($state);

    /**
     * @param $address
     * @return StorelocatorInterface
     * Set Address
     */
    public function setAddress($address);

    /**
     * @param $urlKey
     * @return StorelocatorInterface
     * Set Url Key
     */
    public function setUrlKey($urlKey);

    /**
     * @param $workSchedule
     * @return StorelocatorInterface
     * Set Work Schedule
     */
    public function setWorkSchedule($workSchedule);

    /**
     * @return StorelocatorInterface
     * @param $longitude
     * Set Longitude
     */
    public function setLongitude($longitude);

    /**
     * @return StorelocatorInterface
     * @param $latitude
     * Set Latitude
     */
    public function setLatitude($latitude);
}
