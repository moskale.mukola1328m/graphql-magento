<?php

namespace Elogic\Storelocator\Block;

use Elogic\Storelocator\Model\StorelocatorRepository;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\Template;
use Magento\UrlRewrite\Model\UrlRewriteFactory;

class Stores extends Template
{
    /**
     * @var UrlInterface
     */
    protected $urlBuilder;
    /**
     * @var RequestInterface
     */
    protected $request;
    /**
     * @var StorelocatorRepository
     */
    private $storelocatorRepository;

    /**
     * Storelocator constructor.
     * @param StorelocatorRepository $storelocatorRepository
     * @param Template\Context $context
     * @param UrlInterface $urlBuilder
     * @param RequestInterface $request
     * @param array $data
     */
    public function __construct(
        StorelocatorRepository $storelocatorRepository,
        Template\Context $context,
        UrlInterface $urlBuilder,
        RequestInterface $request,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->urlBuilder = $urlBuilder;
        $this->request = $request;
        $this->storelocatorRepository = $storelocatorRepository;
    }

    public function getStore()
    {
        $urlKey = $this->request->getParam('url_key');
        return $this->storelocatorRepository->getByUrlKey($urlKey);
    }
}
