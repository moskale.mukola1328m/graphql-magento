<?php

namespace Elogic\Storelocator\Block\Adminhtml\Storelocator\Edit;

use Elogic\Storelocator\Api\StorelocatorRepositoryInterface;
use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class GenericButton
 */
class GenericButton
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * @var StorelocatorRepositoryInterface
     */
    protected $storelocatorRepository;

    /**
     * @param Context $context
     * @param StorelocatorRepositoryInterface $storelocatorRepository
     */
    public function __construct(
        Context $context,
        StorelocatorRepositoryInterface $storelocatorRepository
    ) {
        $this->context = $context;
        $this->storelocatorRepository = $storelocatorRepository;
    }

    /**
     * @return int|null
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getStoreId()
    {
        if ($this->context->getRequest()->getParam('store_id')) {
            try {
                return $this->storelocatorRepository->getById(
                    $this->context->getRequest()->getParam('store_id')
                )->getId();
            } catch (NoSuchEntityException $e) {
            }
        }

        return null;
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
