<?php

namespace Elogic\Storelocator\Block;

use Elogic\Storelocator\Helper\Data;
use Elogic\Storelocator\Model\ResourceModel\Storelocator\CollectionFactory;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\Template;

class StorelocatorBlock extends Template
{
    const PAGER = [3 => 3];

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;
    /**
     * @var Data
     */
    protected $helper;
    /**
     * @var UrlInterface
     */
    protected $urlBuilder;
    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * Storelocator constructor.
     * @param CollectionFactory $collectionFactory
     * @param Template\Context $context
     * @param Data $helper
     * @param UrlInterface $urlBuilder
     * @param RequestInterface $request
     * @param array $data
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        Template\Context $context,
        Data $helper,
        UrlInterface $urlBuilder,
        RequestInterface $request,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->collectionFactory = $collectionFactory;
        $this->helper = $helper;
        $this->urlBuilder = $urlBuilder;
        $this->request = $request;
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        if ($this->getStores()) {
            $pager = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'pager'
            )->setAvailableLimit(self::PAGER)->setShowPerPage(true)->setCollection(
                $this->getStores()
            );
            $this->setChild('pager', $pager);
            $this->getStores()->load();
        }
        return $this;
    }

    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    public function getStores()
    {
        $page=($this->getRequest()->getParam('p')) ? $this->getRequest()->getParam('p') : 1;
        $collection = $this->collectionFactory->create();

        $request = $this->request->getParam('store_name');
        if (!empty($request)) {
            $collection->addFieldToFilter('store_name', ['like' => '%' . $request . '%']);
        }

        $collection->setOrder('store_id', 'ASC');
        $collection->setPageSize(3);
        $collection->setCurPage($page);
        return $collection;
    }

    public function getFirstImage(string $images)
    {
        return $this->urlBuilder->getBaseUrl() . 'pub/media/storelocator/images/' . explode(';', $images)[0];
    }

    public function getLocatorUrl()
    {
        return $this->urlBuilder->getUrl('storelocator');
    }

    public function getStoreUrl($urlKey)
    {
        return $this->urlBuilder->getUrl('storelocator/stores') . $urlKey;
    }
}
